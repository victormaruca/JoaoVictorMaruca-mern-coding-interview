import { JsonController, Get, Put, Body, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService();

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Put('/:code')
    async updateStatus(@Param('code') code: String, @Body() body: any) {
        return {
            status: 200,
            data: await flightsService.updateStatus(code, body.status),
        }
    }
}
