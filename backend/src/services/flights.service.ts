import { FlightsModel, FlightStatuses } from '../models/flights.model'
export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async getFlightByCode(code: String) {
        return await FlightsModel.find({ code })
    }

    async updateStatus(code: String, status: FlightStatuses) {
        return FlightsModel.updateOne(
            { code },
            { status }
        )
    }
}
